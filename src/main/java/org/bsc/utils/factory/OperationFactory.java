package org.bsc.utils.factory;

import org.bsc.model.Operation;
import org.bsc.utils.factory.impl.MinusOperation;
import org.bsc.utils.factory.impl.PlusOperation;

/**
 * Factory class to determine which method class to use
 */
public class OperationFactory {

    public static OperationMethod getFactory(Operation operation) {
        switch (operation) {
            case PLUS:
                return new PlusOperation();
            case MINUS:
                return new MinusOperation();
            default:
                return new PlusOperation(); //or default method,
        }
    }

}
