package org.bsc.utils.factory.impl;

import org.bsc.utils.factory.OperationMethod;

/**
 * Plus factory operation method class
 */
public class PlusOperation implements OperationMethod {

    //@NonNull check
    @Override
    public Integer doOperation(Integer value1, Integer value2) {
        return value1 + value2;
    }

}
