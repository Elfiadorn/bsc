package org.bsc.utils.factory;

public interface OperationMethod {

    Integer doOperation(Integer value1, Integer value2);

}
