package org.bsc.utils;

import org.bsc.model.Operation;

/**
 * Number utils do all stuff with numbers
 */
public class NumberUtils {

    public static Integer getInteger(String value) throws Exception {
        try {
            Integer result = Integer.parseInt(value);
            return Math.abs(result);
        } catch (NumberFormatException nfe) {
            throw new Exception(nfe.getMessage());
        }
    }

    public static Operation getOperation(String value) throws Exception {
        try {
            Integer result = Integer.parseInt(value);
            return result < 0 ? Operation.MINUS : Operation.PLUS;
        } catch (NumberFormatException nfe) {
            throw new Exception(nfe.getMessage());
        }
    }

}
