package org.bsc.utils;

/**
 * String utils do all stuff with strings
 */
public class StringUtils {

    public static final boolean isEmpty(String value){
        return value == null || value.length() == 0;
    }

    /**
     * @param value - input value
     * @param size - check lenght of value
     * @return UpperCase of value
     */
    public static String checkAndFormat(String value, int size) throws Exception {
        if(value.length() == size){
            return value.toUpperCase();
        }

        throw new Exception("The lenght of input is different than" + size);
    }
}
