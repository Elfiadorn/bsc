package org.bsc;

import org.bsc.model.Payment;
import org.bsc.service.command.CommandService;
import org.bsc.service.command.impl.CommandServiceImpl;
import org.bsc.service.currency.CurrencyParserService;
import org.bsc.service.currency.impl.CurrencyParserServiceImpl;
import org.bsc.service.payment.PaymentService;
import org.bsc.service.payment.impl.PaymentServiceImpl;

import java.util.Scanner;

/**
 * Main class
 *
 * In real projects will all references of services be included by old methods
 * @Autowired or by new way - constructor
 */
public class App 
{
    public static void main( String[] args )
    {
        //Spring Autowired
        final Scanner in = new Scanner(System.in);
        final CurrencyParserService currencyParserService = new CurrencyParserServiceImpl();
        final PaymentService paymentService = new PaymentServiceImpl();
        final CommandService commandService = new CommandServiceImpl();

        //read input
        while (true) {
            if (in.hasNext()) {
                try {
                    String text = in.nextLine();

                    if(commandService.isCommand(text)){
                        commandService.executeCommand(text);
                    } else {
                        Payment payment = currencyParserService.parseInput(text);
                        paymentService.makePayment(payment);
                    }
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                }
            }
        }
    }
}
