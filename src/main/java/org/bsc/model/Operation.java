package org.bsc.model;

/**
 * Operation enum class, can be in future extended
 */
public enum Operation {

    PLUS,
    MINUS

}
