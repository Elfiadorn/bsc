package org.bsc.model;

/**
 * Payment object contains currency, operation and value
 */
public class Payment {

    private Currency currency;
    private Integer value;
    private Operation operation;

    public Payment(Currency currency, Integer value, Operation operation) {
        this.currency = currency;
        this.value = value;
        this.operation = operation;
    }

    public Currency getCurrency() {
        return currency;
    }

    public Integer getValue() {
        return value;
    }

    public Operation getOperation() {
        return operation;
    }

    //Test purpose
    @Override
    public String toString() {
        return "Payment{" +
                "currency=" + currency +
                ", value=" + value +
                ", operation=" + operation +
                '}';
    }
}
