package org.bsc.model;

/**
 * Represents currency object
 */
public class Currency extends AbstractModel {

    private String name;

    public Currency(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}
