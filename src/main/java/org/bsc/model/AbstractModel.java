package org.bsc.model;

/**
 * Abstract model class include id value
 */
public class AbstractModel {

    private static Integer idGenerator = 0;

    private Integer id;

    public Integer getId() {
        return id;
    }

    //generate unique id
    public AbstractModel() {
        id = idGenerator++;
    }
}
