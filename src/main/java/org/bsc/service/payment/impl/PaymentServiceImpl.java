package org.bsc.service.payment.impl;

import org.bsc.backend.Backend;
import org.bsc.model.Payment;
import org.bsc.service.payment.PaymentService;

/**
 * Payment service which delegate operations or payments
 */
public class PaymentServiceImpl implements PaymentService {

    private static final Backend backend = Backend.getBackend();

    @Override
    public void makePayment(Payment payment) {
        backend.makeOperation(payment);
    }

}
