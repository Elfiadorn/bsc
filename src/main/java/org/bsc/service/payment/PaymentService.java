package org.bsc.service.payment;

import org.bsc.model.Payment;

public interface PaymentService {

    void makePayment(Payment payment);

}
