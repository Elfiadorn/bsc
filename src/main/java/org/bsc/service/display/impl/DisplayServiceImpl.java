package org.bsc.service.display.impl;

import org.bsc.backend.Backend;
import org.bsc.service.display.DisplayService;

/**
 * Easy display service to show all values
 */
public class DisplayServiceImpl implements DisplayService {

    private static final Backend backend = Backend.getBackend();

    @Override
    public void show() {
        backend.show();
    }

}
