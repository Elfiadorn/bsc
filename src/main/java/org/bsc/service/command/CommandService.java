package org.bsc.service.command;

public interface CommandService {

    boolean isCommand(String text);

    void executeCommand(String command);

}
