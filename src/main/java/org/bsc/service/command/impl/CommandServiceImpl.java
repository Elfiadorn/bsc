package org.bsc.service.command.impl;

import org.bsc.backend.Backend;
import org.bsc.service.command.CommandService;

import java.util.ArrayList;
import java.util.List;

/**
 * Command service handle input commands
 */
public class CommandServiceImpl implements CommandService {

    private static final String SHOW = "show";
    private static final String COURSE = "course";

    private static final List<String> COMMANDS = new ArrayList<>();

    static {
        COMMANDS.add(SHOW);
        COMMANDS.add(COURSE);
    }

    //@NonNull check
    @Override
    public boolean isCommand(String text) {
        //FluentIterable or Java8 stream - map
        for (String command : COMMANDS) {
            if(text.contains(command))
                return true;
        }
        return false;
    }

    //@NonNull check
    @Override
    public void executeCommand(String command) {
        if (command.contains(SHOW)){
            Backend.getBackend().show();
        }

        if (command.contains(COURSE)){
            //set course
        }
    }

}
