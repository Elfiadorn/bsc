package org.bsc.service.currency.impl;

import org.bsc.model.Currency;
import org.bsc.service.currency.CurrencyService;

import java.util.HashMap;
import java.util.Map;

//Better approach singleton - or spring autowired
/**
 * Service handling all currency operations (inserting, getting instances, ... in future more)
 */
public class CurrencyServiceImpl implements CurrencyService {

    private final Object LOCK = new Object(){};
    private static final Map<String, Currency> currencyMap = new HashMap<>();

    //@NonNull check
    @Override
    public Currency getCurrency(String name) {
        synchronized (LOCK) {
            return currencyMap.containsKey(name) ? currencyMap.get(name) : insertNewCurrency(name);
        }
    }

    //@NonNull check
    @Override
    public Currency getCurrency(Integer id) throws Exception {
        synchronized (LOCK) {
            for (Map.Entry<String, Currency> entry : currencyMap.entrySet()) {
                if (entry.getValue().getId() == id) {
                    return entry.getValue();
                }
            }
            throw new Exception("Unknown type of currency");
        }
    }

    //@NonNull check
    private Currency insertNewCurrency(String name) {
        synchronized (LOCK) {
            Currency currency = new Currency(name);
            currencyMap.put(name, currency);
            return currency;
        }
    }

}
