package org.bsc.service.currency.impl;

import org.bsc.model.Currency;
import org.bsc.model.Operation;
import org.bsc.model.Payment;
import org.bsc.service.currency.CurrencyParserService;
import org.bsc.service.currency.CurrencyService;
import org.bsc.utils.NumberUtils;
import org.bsc.utils.StringUtils;

/**
 * Service handling currency parsing from input
 */
public class CurrencyParserServiceImpl implements CurrencyParserService {

    private CurrencyService currencyService = new CurrencyServiceImpl();

    //@NonNull check
    @Override
    public Payment parseInput(String input) throws Exception {
        if(StringUtils.isEmpty(input)){
            throw new Exception("Input is empty");
        }

        String[] split = input.split("\\s+");

        if(split.length == 2){
            Currency currency = currencyService.getCurrency(StringUtils.checkAndFormat(split[0], 3));
            Integer value = NumberUtils.getInteger(split[1]);
            Operation operation = NumberUtils.getOperation(split[1]);

            return new Payment(currency, value, operation);
        } else {
            throw new Exception("Format of input is wrong.");
        }
    }

}
