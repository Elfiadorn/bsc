package org.bsc.service.currency;

import org.bsc.model.Currency;

public interface CurrencyService {

    Currency getCurrency(String name);

    Currency getCurrency(Integer id) throws Exception;



}
