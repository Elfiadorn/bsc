package org.bsc.service.currency;

import org.bsc.model.Payment;

public interface CurrencyParserService {

    Payment parseInput(String input) throws Exception;

}
