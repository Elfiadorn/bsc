package org.bsc.backend;

import org.bsc.model.Currency;
import org.bsc.model.Payment;
import org.bsc.service.currency.CurrencyService;
import org.bsc.service.currency.impl.CurrencyServiceImpl;
import org.bsc.utils.factory.OperationFactory;
import org.bsc.utils.factory.OperationMethod;

import java.util.HashMap;
import java.util.Map;

/**
 * Backend improvisation - in real systems some database and ORM (hibernate, eclipselink, ...)
 */
public class Backend {

    private static Backend INSTANCE;
    private static final CurrencyService currencyService = new CurrencyServiceImpl();

    private Backend(){}

    public static Backend getBackend(){
        if(INSTANCE == null){
            INSTANCE = new Backend();
        }
        return INSTANCE;
    }

    private static final Object LOCK = new Object() {};

    private static final Map<Integer, Integer> actualData = new HashMap<>();

    //@NonNull check
    public void makeOperation(Payment payment) {
        synchronized (LOCK) {
            Integer id = payment.getCurrency().getId();
            actualData.put(id, getNewValue(payment));
        }
    }

    //@NonNull check
    private Integer getNewValue(Payment payment) {
        Integer id = payment.getCurrency().getId();
        OperationMethod method = OperationFactory.getFactory(payment.getOperation());
        Integer value1 = actualData.containsKey(id) ? actualData.get(payment.getCurrency().getId()) : 0;
        return method.doOperation(value1, payment.getValue());
    }

    public void show() {
        synchronized (LOCK) {
            for (Map.Entry<Integer, Integer> entry : actualData.entrySet()) {
                try {
                    Currency currency = currencyService.getCurrency(entry.getKey());
                    System.out.println(currency + " " + entry.getValue());
                } catch (Exception e) {
                    //ignore or do something
                }
            }

        }
    }
}
